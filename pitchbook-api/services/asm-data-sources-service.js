(function (context) {
    'use strict';

    var DataSourcesService = {
    };

    var _db;

    var _resolveDb;
    var _rejectDb;
    var _dbIntializedPromise = new Promise(function (resolve, reject) {
        _resolveDb = resolve;
        _rejectDb = reject;
    });


    /**
     * NOTE: This method is internal and should only be called by the API interally
     * Initalize the data sources for the pitch
     * @param {any} data
     * @private
     */
    DataSourcesService._initializeData = function _initializeData(pitch) {
        if (!_db) {
            _db = {};
           ASM.InteropService.getConfig().then(function success(config) {
                if (config.dataSources) {
                    for (var i = 0; i < config.dataSources.length; i++) {
                        const dataSource = config.dataSources[i];


                        // Assign the data source to the database
                        if (pitch && pitch.excels && pitch.excels[dataSource.dataSourceName]) {
                            _db[dataSource.filteredDataName] = pitch.excels[dataSource.dataSourceName];
                        } else if (dataSource.mockData) {
                            _db[dataSource.filteredDataName] = dataSource.mockData;

                            // Only show the warning if we were initailized with data, otherwise using info
                            if (pitch) {
                                ASM.DebugService.logWarning(
                                    'The filtered data source ' + dataSource.filteredDataName + '(DSN: ' + dataSource.dataSourceName + ') was defined in the pitch-config.json, but was not found in the passsed data!  Using mock data.',
                                    'DSNnotFound');
                            } else {
                                ASM.DebugService.logInfo('Using mock data for the data source ' + dataSource.filteredDataName);
                            }
                        }
                    }
                }
                _resolveDb(_db);
            }, function failure(e) {
                _rejectDb(e);
            });
        } else {
            /*  //Temporary commented as it needs to be fix via pitch editor control changes
            ASM.DebugService.logWarning(
                'DataSourcesService._initializeData was called a second time despite being intiailzed already!',
                'datasourcesAlreadyInitialized');
                */
        }


        return _dbIntializedPromise;
    };


    /**
     * Returns a JSON object containing all the data for the specified filtered data source
     * @returns {any}
     */
    DataSourcesService.getData = function getData(filteredDataSourceName) {
        return new Promise(function (resolve, reject) {
            _dbIntializedPromise.then(function success(db) {
                if (db[filteredDataSourceName]) {
                    resolve(db[filteredDataSourceName]);
                } else {
                   ASM.DebugService.logWarning('The filtered data source name ' + filteredDataSourceName + ' does not exist!',
                        'datasourceNotFound',
                        undefined, e);

                    reject();
                }


            }, function failure(e) {
                ASM.DebugService.logWarning('Failed to load pitch datasources database!',
                    'dbLoadFailed',
                    undefined, e);

                reject(e);
            });
        });
    };

    /**
     * Retursn the complete database.  Use cautiously
     * @returns {any}
     */
    DataSourcesService.getDB = function getDB() {
        return _dbIntializedPromise;
    };

    
    context.DataSourcesService = DataSourcesService;
})(ASM);