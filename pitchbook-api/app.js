// Declare an ASM namespace
const ASM = {
    /**
     * The ASM.ready promise will be fired when the ASM libray has been loaded and is ready to operate.  Ready is fired
     * before data is received for hte pitches, but the config should be ready.
     */
    ready: new Promise(function(resolve, reject){
        setTimeout(function() {
            // This preloads the config file
            ASM.InteropService.getConfig().then(function (config){
                resolve(config);
            }, reject);
        })

    })
};